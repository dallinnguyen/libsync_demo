package com.DefaultCompany.lipsyncdemoSALSA;

import com.unity3d.player.UnityPlayerActivity;
import android.os.Bundle;
import android.util.Log;
import com.ksyun.media.diversity.agorastreamer.agora.*;

public class MainActivity extends UnityPlayerActivity {
	
	    //public RemoteDataObserver mRemoteDataObserver;
		
	public static MainActivity instance() {
    return new MainActivity();
	}
	
  protected void onCreate(Bundle savedInstanceState) {
    // call UnityPlayerActivity.onCreate()
    super.onCreate(savedInstanceState);
    // print debug message to logcat
    Log.d("dallin", "onCreate called!");
  }
      public void enableAudioObserver(){
        Log.d("Dallin", "enableAudioObserver: ");
        RemoteDataObserver mRemoteDataObserver = new RemoteDataObserver();
        mRemoteDataObserver.enableObserver(true);
    }
}
