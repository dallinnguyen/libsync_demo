﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class JoyStickController : MonoSingleton<JoyStickController>
{
    public bl_Joystick movementJoyStick;
    public float speed = 5f;
    public MouseLook mouseLook = new MouseLook();

    private Camera _mainCamera;
    private Transform _target;
    private Rigidbody _rigidBody;

    public void SetTarget(Transform target)
    {
        _target = target;
        _rigidBody = _target.GetComponent<Rigidbody>();
        _mainCamera = _target.Find("MainCamera").GetComponent<Camera>();
        mouseLook.Init(_target, _mainCamera.transform);
    }

    private void Update()
    {
        if (_target != null)
        {
            RotateView();

            //movement
            float v = movementJoyStick.Vertical;
            float h = movementJoyStick.Horizontal;

            Vector3 translate = (new Vector3(h, 0, v) * Time.deltaTime) * speed;
            _target.Translate(translate);
        }
    }

    private void RotateView()
    {
        //avoids the mouse looking if the game is effectively paused
        if (Mathf.Abs(Time.timeScale) < float.Epsilon) return;

        // get the rotation before it's changed
        float oldYRotation = transform.eulerAngles.y;

        mouseLook.LookRotation(_target, _mainCamera.transform);

        // Rotate the rigidbody velocity to match the new direction that the character is looking
        Quaternion velRotation = Quaternion.AngleAxis(transform.eulerAngles.y - oldYRotation, Vector3.up);
        _rigidBody.velocity = velRotation * _rigidBody.velocity;
    }
}
