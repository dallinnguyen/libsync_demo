﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class CustomAudioData
{
    public int StreamID;
    public int LengthSamples;
    public int Channels;
    public int Frequency;
    public byte[] Samples;
    public int OffsetSample;
}

public enum AUDIO_FRAME_TYPE {
    FRAME_TYPE_PCM16,

}

public struct AudioFrame
{
    AUDIO_FRAME_TYPE type;
    int samples;
    int bytesPerSample;
    int channels;
    int samplesPerSec;
    IntPtr buffer;
    Int64 renderTimeMs;
    int avsync_type;
}