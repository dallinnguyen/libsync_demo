﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupManager : MonoSingleton<PopupManager>
{
    [SerializeField]
    CanvasGroup warningPopup;

    [SerializeField]
    Button warningPopupOkBtn;

    public int connectedUser = 0;

    void Start() {
        warningPopupOkBtn.onClick.AddListener(onCloseWarningPopupButtonClicked);
    }

    // Start is called before the first frame update
    public void showWarningPopup(bool value) {
        warningPopup.gameObject.SetActive(value);

        if (value)
        {
            warningPopup.interactable = true;
            warningPopup.blocksRaycasts = true;
        }
        else {
            warningPopup.interactable = false;
            warningPopup.blocksRaycasts = false;
        }
    }

    private void onCloseWarningPopupButtonClicked() {
        showWarningPopup(false);
    }
}
