﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EServerRegion
{
    Auto,
    USWest,
    USEast,
    Singapore,
    China,
    EU,
    Japan

}

public class EnterNameState : BaseState
{

    //[SerializeField]
    //InputField m_userName;

    [SerializeField]
    Button m_joinRoomBtn;

    [SerializeField]
    private Dropdown _regionDropDown;

    private List<string> _regionList = new List<string>();



    // Start is called before the first frame update
    void Start()
    {
        m_joinRoomBtn.onClick.AddListener(onJoinRoomButtonClicked);


        foreach (EServerRegion region in System.Enum.GetValues(typeof(EServerRegion))) {
            _regionList.Add(region.ToString());
        }

        _regionDropDown.AddOptions(_regionList);

        _regionDropDown.onValueChanged.AddListener(delegate {
            regionDropdownValueChanged();
        });

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnEnter()
    {
        base.OnEnter();

    }

    public override void OnExit()
    {
        base.OnExit();
    }

    public void onJoinRoomButtonClicked() {

        //string username = m_userName.text.Trim();

        //if (username == string.Empty) return;

        //generate uid, optimize for real product
        uint udid = (uint)Random.Range(100000, 999999);

        GameState.Instance.UDID = udid;

        if (GameState.Instance.mRtcEngine == null) {
            Debug.LogError("mRtcEngine is null");
            return;
        }

        Debug.Log("dallin join channel with id " + udid);

        //everyone join test channel
        GameState.Instance.mRtcEngine.JoinChannel("dallin_test", string.Empty, udid);

        GameState.Instance.PushState(EGameState.ChatRoomState);
    }

    private void regionDropdownValueChanged() {
        GameManager.Instance.currentRegion = (EServerRegion)_regionDropDown.value;
    }
}
