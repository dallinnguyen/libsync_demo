﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityStandardAssets.Cameras;
using agora_gaming_rtc;
public class NetworkCharacter : MonoBehaviourPun, IPunInstantiateMagicCallback, IPunObservable
{
    //references
    public GameObject characterCamera;
    public OVRLipSyncContext context;
    public OVRLipSyncMicInput micInput;
    public AudioSource audioSource;
    public OVRLipSyncContextMorphTarget morphTarget;
    public OVRLipSyncContextTextureFlip textureFlip;

    //transferred data
    public string frameData;
    public int smoothAmount;

    public uint UDID;

    private EHeadType currentType = EHeadType.Human;

    private SwitchHead switchHead = null;

    private void Start()
    {
        switchHead = GetComponent<SwitchHead>();
    }

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        if (!photonView.IsMine)
        {
            characterCamera.SetActive(false);
            micInput.enabled = false;
            audioSource.enabled = false;

            GameState.Instance._networkCharacterList.Add(this);

        } else
        {
            Transform[] positions = GameManager.Instance.photonManager.transform.GetComponentsInChildren<Transform>();
            transform.position = positions[Random.Range(0, positions.Length)].position;

            Transform[] childs = GetComponentsInChildren<Transform>();
            foreach (var child in childs) {
                child.gameObject.layer = LayerMask.NameToLayer("SelfOcclude");
            }

            JoyStickController.Instance.transform.Find("Movement").gameObject.SetActive(true);
            JoyStickController.Instance.transform.Find("Rotation").gameObject.SetActive(true);
            JoyStickController.Instance.SetTarget(transform);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext((int)currentType);
            stream.SendNext(morphTarget.smoothAmount);
            stream.SendNext(JsonUtility.ToJson(context.GetCurrentPhonemeFrame()));
        } else
        {
            object agoraId = photonView.Owner.CustomProperties["agoraId"];
            if (agoraId != null)
                UDID = uint.Parse(agoraId.ToString());

            //synced head
            EHeadType networkHead = (EHeadType)((int)stream.ReceiveNext());
            if (networkHead != currentType)
            {
                currentType = networkHead;
                switchHead.switchHead(currentType);
            }

            //sync smoothing
            smoothAmount = (int)stream.ReceiveNext();
            context.Smoothing = smoothAmount;

            //sync lipsync data
            frameData = (string)stream.ReceiveNext();

            OVRLipSync.Frame frame = JsonUtility.FromJson<OVRLipSync.Frame>(frameData);

            if (frame != null)
            {
                if (currentType == EHeadType.Human)
                {
                    morphTarget.SetVisemeToMorphTarget(frame);

                    morphTarget.SetLaughterToMorphTarget(frame);
                } else
                {
                    // Perform smoothing here if on original provider
                    if (context.provider == OVRLipSync.ContextProviders.Original)
                    {
                        // Go through the current and old
                        for (int i = 0; i < frame.Visemes.Length; i++)
                        {
                            // Convert 1-100 to old * (0.00 - 0.99)
                            float smoothing = ((smoothAmount - 1) / 100.0f);
                            textureFlip.oldFrame.Visemes[i] =
                                textureFlip.oldFrame.Visemes[i] * smoothing +
                                frame.Visemes[i] * (1.0f - smoothing);
                        }
                    }
                    else
                    {
                        textureFlip.oldFrame.Visemes = frame.Visemes;
                    }

                    textureFlip.SetVisemeToTexture();
                }
            }

            double pan = GameState.Instance.getPanFromUser(UDID);
            double gain = GameState.Instance.getGainFromUser(UDID);

            //push pan and gain to agora
            GameState.Instance.mRtcEngine.GetAudioEffectManager().SetRemoteVoicePosition(UDID, pan, gain) ;


        }
    }

    public void switchMyHead() {
        if (switchHead == null)
            switchHead = GetComponent<SwitchHead>();

        if (currentType == EHeadType.Human) currentType = EHeadType.Robot; else currentType = EHeadType.Human;

        switchHead.switchHead(currentType);
    }
}
