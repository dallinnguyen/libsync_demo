﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

using agora_gaming_rtc;

#if(UNITY_2018_3_OR_NEWER)
using UnityEngine.Android;
#endif

//using CrazyMinnow.SALSA;

public enum EGameState
{
    EnterNameState,
    ChatRoomState
}
public class GameState : MonoSingleton<GameState>
{

    public event Action<EGameState> OnStateChange;

    [SerializeField]
    private EGameState m_DefaultState;

    [SerializeField]
    private BaseState m_enterNameState;

    [SerializeField]
    private BaseState m_chatRoomState;

    public NetworkCharacter _mainCharacter;

    public EGameState CurrentState { get; private set; }

    private readonly Stack<EGameState> m_StateStack = new Stack<EGameState>();
    private BaseState m_CurrentState;

    public IRtcEngine mRtcEngine = null;

    //public agora_gaming_rtc engine = null;

    private string appId = "4133ccb0f5c5455a98d8d2a9ef00dc4b";

    string debugTag = "dallin ";

    // Use this for initialization
    private ArrayList permissionList = new ArrayList();

    public bool IsPermissionChecked = false;

    AudioSource _source;

    //private Salsa2D salsa2D; // Reference to the Salsa3D class

    public uint UDID;

    public int position = 0;
    public int samplerate = 44100;
    public float frequency = 440;

    public int mDataStreamId;

    public List<NetworkCharacter> _networkCharacterList = new List<NetworkCharacter>();



    private void Awake()
    {
    }

    private void Start()
    {
        m_StateStack.Push(m_DefaultState);
        SetState(m_DefaultState);



#if (UNITY_2018_3_OR_NEWER)
        permissionList.Add(Permission.Microphone);
        permissionList.Add(Permission.Camera);
#endif

        CheckPermission();

        Debug.Log("dallin get engine IRtcEngine " + appId);

        // if (mRtcEngine != null) IRtcEngine.Destroy();

        reloadEngine();

    }

    public void reloadEngine() {

        unloadEngine();

        initEngine();

    }

    public void unloadEngine() {
        if (mRtcEngine != null)
        {
            IRtcEngine.Destroy();
            mRtcEngine = null;
        }
    }

    public void initEngine() {
        mRtcEngine = IRtcEngine.GetEngine(appId);

        // mRtcEngine.SetLogFilter(LOG_FILTER.INFO);

        // mRtcEngine.setLogFile("path_to_file_unity.log");


        
            //mRtcEngine.EnableDualStreamMode(true);
         mRtcEngine.SetClientRole(CLIENT_ROLE.BROADCASTER);

        //mRtcEngine.SetChannelProfile(CHANNEL_PROFILE.GAME_FREE_MODE);

        mDataStreamId = GameState.Instance.mRtcEngine.CreateDataStream(true, true);

        mRtcEngine.OnJoinChannelSuccess += (string channelName, uint uid, int elapsed) => {
            string joinSuccessMessage = string.Format(debugTag + "joinChannel callback uid: {0}, channel: {1}, version: {2}", uid, channelName, IRtcEngine.GetSdkVersion());
           // Debug.Log(joinSuccessMessage);

            //local client
            switch (GameManager.Instance.currentRegion) {
                case EServerRegion.Auto:
                    PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "";
                    break;
                case EServerRegion.Singapore:
                    PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "asia";
                    break;
                case EServerRegion.USEast:
                    PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "us";
                    break;
                case EServerRegion.USWest:
                    PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "usw";
                    break;
                case EServerRegion.EU:
                    PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "eu";
                    break;
                case EServerRegion.China:
                    PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "cn";
                    break;
                case EServerRegion.Japan:
                    PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "jp";
                    break;
            }
            PhotonNetwork.ConnectUsingSettings();
        };

        //mRtcEngine.EnableVideo();
        //mRtcEngine.EnableVideoObserver();

        mRtcEngine.OnLeaveChannel += (RtcStats stats) => {
            string leaveChannelMessage = string.Format(debugTag + "onLeaveChannel callback duration {0}, tx: {1}, rx: {2}, tx kbps: {3}, rx kbps: {4}", stats.duration, stats.txBytes, stats.rxBytes, stats.txKBitRate, stats.rxKBitRate);
            //Debug.Log(leaveChannelMessage);

            reloadEngine();
        };

        mRtcEngine.OnUserJoined += (uint uid, int elapsed) => {
            string userJoinedMessage = string.Format(debugTag + "onUserJoined callback uid {0} {1}", uid, elapsed);
            //Debug.Log(userJoinedMessage);

            //remote client
            //GameManager.Instance.createAvatar(uid, string.Empty);

        };

        mRtcEngine.OnUserOffline += (uint uid, USER_OFFLINE_REASON reason) => {
            string userOfflineMessage = string.Format(debugTag + "onUserOffline callback uid {0} {1}", uid, reason);
            //Debug.Log(userOfflineMessage);

            GameManager.Instance.UserLeaveChannel(uid);
        };

        mRtcEngine.OnVolumeIndication += (AudioVolumeInfo[] speakers, int speakerNumber, int totalVolume) => {
            if (speakerNumber == 0 || speakers == null)
            {
                //Debug.Log(string.Format(debugTag + "onVolumeIndication only local {0}", totalVolume));
            }

            for (int idx = 0; idx < speakerNumber; idx++)
            {
                string volumeIndicationMessage = string.Format(debugTag + "{0} onVolumeIndication {1} {2}", speakerNumber, speakers[idx].uid, speakers[idx].volume);
                //Debug.Log(volumeIndicationMessage);
            }
        };

        mRtcEngine.OnStreamMessageError += (uint userId, int streamId, int code, int missed, int cached) =>
        {
            Debug.Log("OnStreamMessageError " + code);

        };

        mRtcEngine.OnStreamMessage += (uint uid, int streamId, string data, int length) => {
            //Debug.Log("dallin OnStreamMessage working");
            //Debug.Log(string.Format(debugTag + "{0} {1} OnStreamMessage {2} {3}", uid, streamId, length, data));



            //AudioClip randomClip = AudioClip.Create(string.Empty, samplerate * 2, 1, samplerate, true, OnAudioRead, OnAudioSetPosition);

            //int index = -1;
            //Int32.TryParse(data, out index);

            //GameManager.Instance.MessageReceived(uid, index);

            //deserialize data
            //CustomAudioData customData = null;

            //try
            //{
            //    customData = JsonUtility.FromJson<CustomAudioData>(data);
            //}catch(Exception e)
            //{
            //    Debug.LogError("dallin OnStreamMessage deserialize failed");
            //    Debug.LogError(e);
            //}

            //if (customData != null) {
            //    Debug.Log("dallin get data successfully");
            //    //get audio clip here
            //    AudioClip streamClip = AudioClip.Create("streamData", customData.LengthSamples, customData.Channels, customData.Frequency, true);

            //    //send to chatroom to update avatar
            //    if (CurrentState == EGameState.ChatRoomState) {
            //        GameManager.Instance.MessageReceived(uid, streamClip);
            //    }
            //}
        };

        mRtcEngine.OnUserMuted += (uint uid, bool muted) => {
            string userMutedMessage = string.Format(debugTag + "onUserMuted callback uid {0} {1}", uid, muted);
            //Debug.Log(userMutedMessage);
        };

        mRtcEngine.OnWarning += (int warn, string msg) => {
            string description = IRtcEngine.GetErrorDescription(warn);
            string warningMessage = string.Format(debugTag + "onWarning callback {0} {1} {2}", warn, msg, description);
            //Debug.Log(warningMessage);
        };

        mRtcEngine.OnError += (int error, string msg) => {
            string description = IRtcEngine.GetErrorDescription(error);
            string errorMessage = string.Format(debugTag + "onError callback {0} {1} {2}", error, msg, description);
            //Debug.Log(errorMessage);
        };

        mRtcEngine.OnRtcStats += (RtcStats stats) => {
            string rtcStatsMessage = string.Format(debugTag + "onRtcStats callback duration {0}, tx: {1}, rx: {2}, tx kbps: {3}, rx kbps: {4}, tx(a) kbps: {5}, rx(a) kbps: {6} users {7}",
                stats.duration, stats.txBytes, stats.rxBytes, stats.txKBitRate, stats.rxKBitRate, stats.txAudioKBitRate, stats.rxAudioKBitRate, stats.users);
            //Debug.Log(rtcStatsMessage);

            int lengthOfMixingFile = mRtcEngine.GetAudioMixingDuration();
            int currentTs = mRtcEngine.GetAudioMixingCurrentPosition();

            //string mixingMessage = string.Format(debugTag + "Mixing File Meta {0}, {1}", lengthOfMixingFile, currentTs);
           // Debug.Log(mixingMessage);
        };

        mRtcEngine.OnAudioRouteChanged += (AUDIO_ROUTE route) => {
           //string routeMessage = string.Format(debugTag + "onAudioRouteChanged {0}", route);
            //Debug.Log(routeMessage);
        };

        mRtcEngine.OnRequestToken += () => {
            string requestKeyMessage = string.Format(debugTag + "OnRequestToken");
            //Debug.Log(requestKeyMessage);
        };

        mRtcEngine.OnConnectionInterrupted += () => {
            string interruptedMessage = string.Format(debugTag + "OnConnectionInterrupted");
            //Debug.Log(interruptedMessage);
        };

        mRtcEngine.OnConnectionLost += () => {
            string lostMessage = string.Format(debugTag + "OnConnectionLost");
            //Debug.Log(lostMessage);
        };




    }

    //private bool isUserAlreadyRegistered(uint uid) {
    //    for (int i = 0; i < _userInfoList.Count; i++) {
    //        if (_userInfoList[i].userID == uid)
    //            return true;
    //    }
    //    return false;
    //}

    public void sendChannelMsg(string message) {

        if (mDataStreamId <= 0)
        {
            mDataStreamId = mRtcEngine.CreateDataStream(true, true); // boolean reliable, boolean ordered
        }

        if (mDataStreamId < 0)
        {
            String errorMsg = "dallin onstreammessage Create data stream error happened " + mDataStreamId;
            Debug.LogError(errorMsg);
            return;
        }

        mRtcEngine.SendStreamMessage(mDataStreamId, message);

    }

    void OnAudioRead(float[] data)
    {
        int count = 0;
        while (count < data.Length)
        {
            data[count] = Mathf.Sin(2 * Mathf.PI * frequency * position / samplerate);
            position++;
            count++;
        }
    }

    void OnAudioSetPosition(int newPosition)
    {
        position = newPosition;
    }

    private void CheckPermission()
    {
#if (UNITY_2018_3_OR_NEWER)
        foreach (string permission in permissionList)
        {
            if (!Permission.HasUserAuthorizedPermission(permission))
            {
                Permission.RequestUserPermission(permission);
            }
        }
#endif
    }

    
    private void Update()
    {
        if (m_CurrentState)
            m_CurrentState.OnUpdate();

    }

    

    private void OnGUI()
    {
        if (m_CurrentState)
            m_CurrentState.OnDraw();
    }

    public void PushState(EGameState state)
    {
        if (CurrentState == state)
            return;

        m_StateStack.Push(state);
        SetState(state);
    }

    public void PopState()
    {
        if (m_StateStack.Count <= 1)
        {
            Debug.LogError("No previous state");
            return;
        }

        m_StateStack.Pop();
        EGameState state = m_StateStack.Peek();
        SetState(state);
    }

    public void PopStateUntilReach(params EGameState[] reeachState)
    {
        EGameState state = m_StateStack.Peek();

        while (!IsContainState(state, reeachState) && m_StateStack.Count > 1)
        {
            m_StateStack.Pop();
            state = m_StateStack.Peek();
        }

        SetState(state);
    }

    public void PopStateUntilReachThenPush(EGameState state, params EGameState[] reachState)
    {
        EGameState checkState = m_StateStack.Peek();

        while (!IsContainState(checkState, reachState) && m_StateStack.Count > 1)
        {
            m_StateStack.Pop();
            checkState = m_StateStack.Peek();
        }

        PushState(state);
    }

    public void PopStateThenPush(EGameState state)
    {
        if (m_StateStack.Count <= 1)
        {
            Debug.LogError("No previous state");
            return;
        }

        m_StateStack.Pop();
        m_StateStack.Push(state);
        SetState(state);
    }

    private bool IsContainState(EGameState state, params EGameState[] stateArray)
    {
        for (int i = 0; i < stateArray.Length; i++)
        {
            if (stateArray[i] == state)
                return true;
        }

        return false;
    }

    private void SetState(EGameState state)
    {
        CurrentState = state;

        if (m_CurrentState)
            m_CurrentState.OnExit();

        switch (state)
        {
            case EGameState.EnterNameState:
                m_CurrentState = m_enterNameState;
                break;

            case EGameState.ChatRoomState:
                m_CurrentState = m_chatRoomState;
                break;

           
        }

        if (m_CurrentState)
            m_CurrentState.OnEnter();

        if (OnStateChange != null)
            OnStateChange(CurrentState);
    }

    public BaseState GetStateObject(EGameState state)
    {
        switch (state)
        {
            case EGameState.EnterNameState:
                return m_enterNameState;

            case EGameState.ChatRoomState:
                return m_chatRoomState;
        }

        return null;
    }
    void OnApplicationQuit()
    {
        unloadEngine();
    }

    //from -1 to 1
    public double getPanFromUser(uint userID)
    {
        double result = -1;

        NetworkCharacter character = getCharacterFromID(userID);

        if (character == null) return -1; // get this value, no need to set anything

        Vector3 targetDir = character.transform.position - _mainCharacter.transform.position;
        double angle = Vector3.SignedAngle(targetDir, transform.forward, Vector3.up);

        result = angle / 180;

        return result;
    }

    //from 0 to 100
    public double getGainFromUser(uint userID)
    {
        double result = -1;

        float maxDistance = GameManager.Instance.maxDistanceToHear;

        NetworkCharacter character = getCharacterFromID(userID);

        if (character == null) return -1; // get this value, no need to set anything

        double distance = Vector3.Distance(character.transform.position, _mainCharacter.transform.position);

        result = 1 - distance / maxDistance;

        if (result < 0)
            result = 0;

        result *= 100;
        result = Math.Floor(result);

        return result;
    }

    private NetworkCharacter getCharacterFromID(uint id) {

        if (_networkCharacterList == null || _networkCharacterList.Count <= 0) return null;


        for (int i = 0; i < _networkCharacterList.Count; i++)
        {
            if (_networkCharacterList[i].UDID == id)
                return _networkCharacterList[i];
        }

        return null;
    }
}