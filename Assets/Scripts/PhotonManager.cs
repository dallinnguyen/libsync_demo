﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityStandardAssets.Cameras;

public class PhotonManager : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    void Start()
    {
        //PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();

        RoomOptions options = new RoomOptions();
        options.MaxPlayers = 3;
        options.EmptyRoomTtl = 0;
        options.PlayerTtl = 0;

        PhotonNetwork.JoinOrCreateRoom("AgoraDemo", options, new TypedLobby("AgoraLobby", LobbyType.Default));
        //PhotonNetwork.CreateRoom("Agora", options, new TypedLobby("AgoraLobby", LobbyType.Default));
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        GameObject obj = PhotonNetwork.Instantiate("Character", new Vector3(0f, 50f, 0f), Quaternion.identity);
        ExitGames.Client.Photon.Hashtable properties = PhotonNetwork.LocalPlayer.CustomProperties;
        properties.Add("agoraId", GameState.Instance.UDID.ToString());
        PhotonNetwork.LocalPlayer.SetCustomProperties(properties);

        NetworkCharacter networkChar = obj.GetComponent<NetworkCharacter>();

        if (networkChar) {
            //save ref of main character
            networkChar.UDID = GameState.Instance.UDID;
            GameState.Instance._mainCharacter = networkChar;
        }
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);

        Debug.LogError(message);
    }
}
